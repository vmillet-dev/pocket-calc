#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPushButton>
#include <QGridLayout>
#include <QTextEdit>
#include <QLabel>
#include <QString>
#include "calcul.h"
class MainWindow : public QMainWindow
{
    Q_OBJECT
private:
    double value = 0.0;
    Calcul calc;
    //Boutons
    QPushButton *button0, *button1, *button2, *button3, *button4, *button5, *button6, *button7, *button8, *button9;
    QPushButton *buttonEnter, *buttonFact ,*buttonDiv, *buttonPlus, *buttonMinus, *buttonExponent, *buttonPoint,
    *buttonBracketLeft, *buttonBracketRight, *buttonClear;

    //Affichage du resultat
    QLabel text, rpnResult;
    QTextEdit *userInput;

    //Organisation de l'affichage
    QWidget *mainWidget, *operatorContener, *winWidget;
    QGridLayout *widgetLayout, *operatorLayout, *winLayout;

private slots:
    void on_button0_clicked();
    void on_button1_clicked();
    void on_button2_clicked();
    void on_button3_clicked();
    void on_button4_clicked();
    void on_button5_clicked();
    void on_button6_clicked();
    void on_button7_clicked();
    void on_button8_clicked();
    void on_button9_clicked();
    void on_buttonEnter_clicked();
    void on_buttonDiv_clicked();
    void on_buttonFact_clicked();
    void on_buttonPlus_clicked();
    void on_buttonMinus_clicked();
    void on_buttonExponent_clicked();
    void on_buttonBracketLeft_clicked();
    void on_buttonBracketRight_clicked();
    void on_buttonClear_clicked();
    void on_buttonPoint_clicked();
public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();
};

#endif // MAINWINDOW_H

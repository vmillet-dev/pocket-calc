#ifndef CALCUL_H
#define CALCUL_H
#include "pile.h"
#include <iostream>
#include <vector>
#include <algorithm>
#include "math.h"
using namespace std;

class Calcul{
private :
    string chain;
    vector<string> postFixExpression;
    Pile pile;

    //retourne vrai si la chaine est valide
    bool isValid(string s);
    void setPostFixExp();
    double convertPostFix();
    int prec(string symbol);
    int prec(char symbol);
    bool isRightAssociative(char chr);
    bool minusIsOperator(int index);
    bool isOperator(char s);
public:
    //Constructeur
    Calcul();
    //Fait le calcul et renvoie la valeur obtenue
    bool doTheMath(string chain, double &result);
    vector<string> getPostFixe(){return this->postFixExpression;}
};

#endif // CALCUL_H

#include "mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    this->setWindowTitle("Pocket-calc");
    this->setFixedSize(0, 0);
    winWidget = new QWidget;
    winLayout = new QGridLayout;

    text.setText("Enter an expression:");
    userInput = new QTextEdit;
    winLayout->setSpacing(0);
    winLayout->addWidget(&text, 0, 0);
    winLayout->addWidget(userInput, 1, 0);
    rpnResult.setText("RPN:");
    rpnResult.setToolTip("Reverse Polish Notation");
    winLayout->addWidget(&rpnResult, 2, 0);

    mainWidget = new QWidget;
    widgetLayout = new QGridLayout;

    //Creation des boutons de l'application
    button0 = new QPushButton("0");
    button1 = new QPushButton("1");
    button2 = new QPushButton("2");
    button3 = new QPushButton("3");
    button4 = new QPushButton("4");
    button5 = new QPushButton("5");
    button6 = new QPushButton("6");
    button7 = new QPushButton("7");
    button8 = new QPushButton("8");
    button9 = new QPushButton("9");
    buttonEnter = new QPushButton("=");
    buttonFact  = new QPushButton("*");
    buttonDiv = new QPushButton("/");
    buttonPlus = new QPushButton("+");
    buttonMinus = new QPushButton("-");
    buttonExponent = new QPushButton("^");
    buttonPoint = new QPushButton(".");
    buttonBracketLeft = new QPushButton("(");
    buttonBracketRight = new QPushButton(")");
    buttonClear = new QPushButton("Clear");

    //On leur ajoute un nom d'objet pour que ces derniers puissent être reconnu par le connecteur de slots
    button0->setObjectName("button0");
    button1->setObjectName("button1");
    button2->setObjectName("button2");
    button3->setObjectName("button3");
    button4->setObjectName("button4");
    button5->setObjectName("button5");
    button6->setObjectName("button6");
    button7->setObjectName("button7");
    button8->setObjectName("button8");
    button9->setObjectName("button9");
    buttonEnter->setObjectName("buttonEnter");
    buttonDiv->setObjectName("buttonDiv");
    buttonFact->setObjectName("buttonFact");
    buttonPlus->setObjectName("buttonPlus");
    buttonMinus->setObjectName("buttonMinus");
    buttonExponent->setObjectName("buttonExponent");
    buttonPoint->setObjectName("buttonPoint");
    buttonBracketLeft->setObjectName("buttonBracketLeft");
    buttonBracketRight->setObjectName("buttonBracketRight");
    buttonClear->setObjectName("buttonClear");

    //Initialisation du layout de la fenetre
    widgetLayout->addWidget(button7, 0, 0);
    widgetLayout->addWidget(button8, 0, 1);
    widgetLayout->addWidget(button9, 0, 2);
    widgetLayout->addWidget(button4, 1, 0);
    widgetLayout->addWidget(button5, 1, 1);
    widgetLayout->addWidget(button6, 1, 2);
    widgetLayout->addWidget(button1, 2, 0);
    widgetLayout->addWidget(button2, 2, 1);
    widgetLayout->addWidget(button3, 2, 2);
    widgetLayout->addWidget(button0, 3, 0);
    widgetLayout->addWidget(buttonPoint, 3, 1);
    widgetLayout->addWidget(buttonEnter, 3, 2);

    operatorContener = new QWidget;
    operatorLayout = new QGridLayout;
    operatorLayout->addWidget(buttonClear, 0, 1);
    operatorLayout->addWidget(buttonDiv, 0, 0);
    operatorLayout->addWidget(buttonExponent, 1, 1);
    operatorLayout->addWidget(buttonFact, 1, 0);
    operatorLayout->addWidget(buttonPlus, 2, 0);
    operatorLayout->addWidget(buttonBracketLeft, 2, 1);
    operatorLayout->addWidget(buttonBracketRight, 3, 1);
    operatorLayout->addWidget(buttonMinus, 3, 0);

    operatorContener->setLayout(operatorLayout);
    widgetLayout->addWidget(operatorContener, 0, 4, 4, 1);
    mainWidget->setLayout(widgetLayout);

    winLayout->addWidget(mainWidget, 3, 0, 1, 1);
    winWidget->setLayout(winLayout);

    this->setCentralWidget(winWidget);

    QMetaObject::connectSlotsByName(this);
}

MainWindow::~MainWindow()
{
    //destruction des boutons
    delete button0;
    delete button1;
    delete button2;
    delete button3;
    delete button4;
    delete button5;
    delete button6;
    delete button7;
    delete button8;
    delete button9;
    delete buttonEnter;
    delete buttonFact;
    delete buttonDiv;
    delete buttonPlus;
    delete buttonMinus;
    delete buttonExponent;
    delete buttonPoint;
    delete buttonBracketLeft;
    delete buttonBracketRight;
    //destructions des composants du clavier numérique
    delete mainWidget;

    //delete widgetLayout;
    //delete operatorContener;
    //delete operatorLayout;

    delete userInput;
}

void MainWindow::on_buttonClear_clicked(){
    userInput->clear();
    rpnResult.clear();
}

void MainWindow::on_button0_clicked(){
    userInput->setText(userInput->toPlainText() + "0");
}

void MainWindow::on_button1_clicked(){
    userInput->setText(userInput->toPlainText() + "1");
}

void MainWindow::on_button2_clicked(){
    userInput->setText(userInput->toPlainText() + "2");
}

void MainWindow::on_button3_clicked(){
    userInput->setText(userInput->toPlainText() + "3");
}

void MainWindow::on_button4_clicked(){
    userInput->setText(userInput->toPlainText() + "4");
}

void MainWindow::on_button5_clicked(){
    userInput->setText(userInput->toPlainText() + "5");
}

void MainWindow::on_button6_clicked(){
    userInput->setText(userInput->toPlainText() + "6");
}

void MainWindow::on_button7_clicked(){
    userInput->setText(userInput->toPlainText() + "7");
}

void MainWindow::on_button8_clicked(){
    userInput->setText(userInput->toPlainText() + "8");
}

void MainWindow::on_button9_clicked(){
    userInput->setText(userInput->toPlainText() + "9");
}

void MainWindow::on_buttonPoint_clicked(){
    userInput->setText(userInput->toPlainText() + ".");
}

void MainWindow::on_buttonEnter_clicked(){
    if(calc.doTheMath(this->userInput->toPlainText().toStdString(), value)){
        userInput->setTextColor(Qt::black);
        userInput->setText(userInput->toPlainText() + QString("=\n")+QString("%1").arg(value));

        int size = calc.getPostFixe().size();
        string text = "RPN:";
        for(int i = 0; i < size; i++){
            text += " " + calc.getPostFixe().at(i);
        }
        rpnResult.setText(QString::fromStdString(text));
    }else{
        userInput->setTextColor(Qt::red);
        userInput->setText(userInput->toPlainText());
    }
}

void MainWindow::on_buttonDiv_clicked(){
    userInput->setText(userInput->toPlainText() + "/");
}

void MainWindow::on_buttonFact_clicked(){
    userInput->setText(userInput->toPlainText() + "*");
}

void MainWindow::on_buttonPlus_clicked(){
    userInput->setText(userInput->toPlainText() + "+");
}

void MainWindow::on_buttonMinus_clicked(){
    userInput->setText(userInput->toPlainText() + "-");
}

void MainWindow::on_buttonExponent_clicked(){
    userInput->setText(userInput->toPlainText() + "^");
}

void MainWindow::on_buttonBracketLeft_clicked(){
    userInput->setText(userInput->toPlainText() + "(");
}

void MainWindow::on_buttonBracketRight_clicked(){
    userInput->setText(userInput->toPlainText() + ")");
}

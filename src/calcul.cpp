#include "calcul.h"

/*
 * Expression qui bug : (5*18)/(7*5)-4*7 ====> 5 18 * 7 5 * / 4 7 * -  ===> -25.4285714286
 */

Calcul::Calcul(){

}
/**
 * @brief   Calcul::isValid Verification du bon "parenthésage" de la chaine, qu'il est une bonne correspondance
 *          entre les délimiteurs '(', ')', '[', ']', '{' et '}'
 * @param s: string
 * @return un boolean si la chaine de caractères est valide
 */
bool Calcul::isValid(string s){
    Pile pile;
    unsigned int i = 0;
    char c;

    for(i = 0; i < s.size(); i++){
        //On empile tous les caractères ouvrants
        if(s.at(i) == '[' || s.at(i) == '(' || s.at(i) == '{'){
            pile.add(s.at(i));
            //si on est face à un crochet fermant, on verifie que le précédent (donc le haut de la pile) soit un ouvrant
        }else if(s.at(i) == ']'){
            //la valeur de l'index 0 est la tete de pile, la dernière entrée et donc la première a sortir
            if(pile.getValueAt(0) != '['){
                return false;
            }else{
                pile.del(c);
            }
            //si on est face à une parenthèse fermante, on verifie que la précédente (donc celle en haut de la pile) soit une ouvrante
        }else if(s.at(i) == ')'){
            if(pile.getValueAt(0) != '('){
                return false;
            }else{
                pile.del(c);
            }
        }else if(s.at(i) == '}'){
            if(pile.getValueAt(0) != '{'){
                return false;
            }else{
                pile.del(c);
            }
        }
    }
    //si la pile n'est pas vide, c'est qu'il y'a un prblèmle
    if(!pile.isEmpty()){
        return false;
    }else{
        //sinon c'est ok
        return true;
    }
}

/**
 * @brief Calcul::prec Permet de tester la priorité de l'operateur entré afin de la comparer par exemple
 * @param symbol
 * @return la valeur de priorité
 */
int Calcul::prec(string symbol)
{
    if(symbol== "(")
        return 0;
    if(symbol== ")")
        return 0;
    if(symbol=="+" || symbol=="-")
        return 1;
    if(symbol=="*" || symbol=="/")
        return 2;
    if(symbol=="^")
        return 3;
    return 0;
}
int Calcul::prec(char symbol)
{
    if(symbol== '(')
        return 0;
    if(symbol== ')')
        return 0;
    if(symbol=='+' || symbol=='-')
        return 1;
    if(symbol=='*' || symbol=='/')
        return 2;
    if(symbol=='^')
        return 3;
    return 0;
}

/**
 * @brief Calcul::isRightAssociative L'associativité détermine l'ordre dans lequel des opérateurs de même précédence
 * sont évalués. Une associativité de droite (droite à gauche) signifie qu'elle est interprétée
 * comme "a OPERATEUR (b OPERATEUR c)" lorsqu'on a "a OP b OP c".
 * Les opérateurs d'affectation sont donc associatifs de droite.
 * @param chr: l'operateur a analyser
 * @return Retourne vrai si l'operateur est de type associatif droit.
 */
bool Calcul::isRightAssociative(char chr){
    return chr == '^';
}

/**
 * @brief Calcul::setPostFixExp Initialise l'expression postfixe de manière à correspondre à la Notation Polonaise inversée, postfix est un
 * conteneur de string, chaque string matérialise un nombre ou un operateur que l'on utilisera pour le calcul.
 * @param chain: string
 */
void Calcul::setPostFixExp(){
    this->postFixExpression.push_back("");
    int j = 0;
    string tmp;
    for(unsigned int i = 0; i < this->chain.size(); i++){
        //si c'est un nombre ou une point, on l'ajoute à la sortie
        if(((int) chain.at(i) >= (int) '0' && (int) chain.at(i) <= (int) '9') || (int)chain.at(i) == (int)'.'){
            this->postFixExpression[j] += chain.at(i);
            //si c'est un opérateur, on l'ajoute dans la pile
        }else if(this->chain.at(i) == '+' || this->chain.at(i) == '/' || this->chain.at(i) == '*' || this->chain.at(i) == '-' || this->chain.at(i) == '^'){
            //si la pile est vide, on l'empile
            if(this->pile.isEmpty()){
                this->pile.add(this->chain.at(i));
                this->postFixExpression.push_back("");
                j++;
            }else{
                if(!minusIsOperator(i) && chain.at(i) == '-'){
                    this->postFixExpression[j] += chain.at(i);
                }else{
                    while(this->prec(this->chain.at(i)) < this->prec(this->pile.s_getValueAt(0)) || (this->prec(this->chain.at(i)) == this->prec(this->pile.s_getValueAt(0)) && !this->isRightAssociative(this->chain.at(i))) && this->pile.s_getValueAt(i) != "("){
                        this->pile.del(tmp);
                        j++;
                        this->postFixExpression.push_back(tmp);
                    }
                    this->pile.add(this->chain.at(i));
                    if(postFixExpression.at(postFixExpression.size() - 1) != ""){
                        this->postFixExpression.push_back("");
                        j++;
                    }
                }

            }
            //si c'est une parenthèse ouvrante
        }else if(this->chain.at(i) == '('){
            //on y met dans la pile
            this->pile.add(this->chain.at(i));
            //si c'est une parenthèse fermante
        }else if(this->chain.at(i) == ')'){
            while(this->pile.s_getValueAt(0) != "("){
                this->pile.del(tmp);
                j++;
                this->postFixExpression.push_back(tmp);
            }
            this->pile.del(tmp);
        }
    }
    //on dépile les derniers opérateurs
    while(pile.count() > 0){
        this->pile.del(tmp);
        j++;
        this->postFixExpression.push_back(tmp);
    }
}

bool Calcul::minusIsOperator(int index){
    if(index == 0 || chain.at(index -1) == '(' || isOperator(chain.at(index - 1))){
        return false;
    }else{
        return true;
    }
}

bool Calcul::isOperator(char s){
    return s == '*' || s == '/' || s == '+' || s == '-' || s == '^';
}

/**
 * @brief Calcul::convertPostFix Permet de convertir l'expression en Notation Polonaise inverse (donc contenu dans la variable postFixExpression)
 * en valeur de type double
 * @return la valeur en double, le resultat donc.
 */
double Calcul::convertPostFix(){
    this->pile.clear();
    string first, second;
    for(unsigned int i = 0; i < this->postFixExpression.size(); i++){
        switch((int) this->postFixExpression.at(i).at(0)){
        case (int)'+':
            if(this->pile.count() >= 2){
                this->pile.del(first);
                this->pile.del(second);
                this->pile.add(to_string(stod(second) + stod(first)));
            }
            break;
        case (int)'-':
            if(this->pile.count() >= 2 && this->postFixExpression.at(i).size() < 2){
                this->pile.del(first);
                this->pile.del(second);
                this->pile.add(to_string(stod(second) - stod(first)));
            }else if(this->postFixExpression.at(i).size() >= 2){
                this->pile.add(this->postFixExpression.at(i));
            }
            break;
        case (int)'*':
            if(this->pile.count() >= 2){
                this->pile.del(first);
                this->pile.del(second);
                this->pile.add(to_string(stod(second) * stod(first)));
            }
            break;
        case (int)'/':
            if(this->pile.count() >= 2){
                this->pile.del(first);
                this->pile.del(second);
                if(stod(first)){
                    this->pile.add(to_string(stod(second) / stod(first)));
                }
            }
            break;
        case (int)'^':
            if(this->pile.count() >= 2){
                this->pile.del(first);
                this->pile.del(second);
                if(stod(first)){
                    this->pile.add(to_string(pow(stod(second), stod(first))));
                }
            }
            break;
        default:
            this->pile.add(this->postFixExpression.at(i));
            break;
        }
    }
    //on retourne la dernière valeur restante, c'est notre résultat
    return atof(this->pile.s_getValueAt(0).c_str());
}

/**
 * @brief Calcul::doTheMath Fait les calculs
 * @param chain: string l'expression à traiter
 * @param result: double le resultat de l'expression
 * @return Renvoie vrai si tout c'est bien déroulé avec le resultat par référence
 */
bool Calcul::doTheMath(string chain, double& result){
    //on test si le parenthésage est valide avant de rentrer dans la boucle
    if(this->isValid(chain)){
        this->postFixExpression.clear();
        this->pile.clear();
        //On supprime les espaces si il y'en a
        chain.erase(std::remove(chain.begin(),chain.end(),' '),chain.end());
        this->chain = chain;
        this->setPostFixExp();
        result =  this->convertPostFix();
        return true;
    }
    return false;
}

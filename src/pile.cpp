#include <pile.h>

Pile::Pile(){
    p = nullptr;
}

//Ajouter un élément en haut de la pile
void Pile::add(char value){
    cellulePile* tmp = new cellulePile();
    tmp->element = value;
    tmp->suivant = p;
    p = tmp;
}

//Supprimer le dernier élément
bool Pile::del(char& value){
    if(!this->isEmpty()){
        value = this->p->element.at(0);
        cellulePile* tmp = this->p;
        this->p = p->suivant;
        delete tmp;
        tmp = nullptr;
        return true;
    }
    return false;
}

//compter le nombre d'éléments
int Pile::count(){
    int cpt = 0;
    cellulePile* tmp = this->p;
    while(p != nullptr){
        p = p->suivant;
        cpt++;
    }
    p = tmp;
    return cpt;
}

//retourne vrai si la liste est vide, faux sinon
 bool Pile::isEmpty(){
    return this->p == nullptr;
 }

 //renvoie la valeur en partant du haut de la pile, renvoie null si la taille est de 0
 char Pile::getValueAt(int index){
     int cpt = 0;
     cellulePile* tmp = this->p;
     while(p != nullptr){
         if(cpt == index){
             cpt = p->element.at(0);
             p = tmp;
             return cpt;
         }
         p = p->suivant;
         cpt++;
     }
     p = tmp;
     return NULL;
 }

void Pile::clear(){
    char c;
    while(!this->isEmpty()){
        this->del(c);
    }
}

void Pile::add(string value){
    cellulePile* tmp = new cellulePile();
    tmp->element = value;
    tmp->suivant = p;
    p = tmp;
}

bool Pile::del(string& value){
    if(!this->isEmpty()){
        value = this->p->element;
        cellulePile* tmp = this->p;
        this->p = p->suivant;
        delete tmp;
        tmp = nullptr;
        return true;
    }
    return false;
}

string Pile::s_getValueAt(int index){
    int cpt = 0;
    string valReturn;
    cellulePile* tmp = this->p;
    while(p != nullptr){
        if(cpt == index){
            valReturn = p->element;
            p = tmp;
            return valReturn;
        }
        p = p->suivant;
        cpt++;
    }
    p = tmp;
    return "";
}

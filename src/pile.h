#ifndef PILE_H
#define PILE_H
#include <iostream>
using namespace std;

typedef struct cellP{
    string element;

    struct cellP * suivant;
}cellulePile;

class Pile{
private:
    cellulePile* p;
public:
    Pile();
    void add(char value);
    bool del(char& value);
    int count();
    bool isEmpty();
    char getValueAt(int index);
    void clear();

    //prise en charge des strings
    void add(string value);
    bool del(string& value);
    string s_getValueAt(int index);
};

#endif // PILE_H

#include "mainwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    std::setlocale(LC_ALL, "en_US.UTF-8");
    std::locale mylocale("en_US.UTF8");   // get global locale
    std::cout.imbue(mylocale);  // imbue global locale

    return a.exec();
}

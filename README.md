# Pocket-calc

It's a simple calculator, just testing my programming's skill and also to pass the time.

# Features 

- Conversion of an arithmetical expression in Reverse Polish Notation (RPN)
- Handle the main operator (+ - / *), with negative number and floating-point numbers.
- Handle the exponent in expression 
- Consideration of the priorities of the brackets 

# License

Licensed under the GPLv3: http://www.gnu.org/licenses/gpl-3.0.html
